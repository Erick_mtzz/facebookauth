import 'package:facebookauth/src/pages/register.dart';
import 'package:facebookauth/src/pages/sign_in.dart';
import 'package:flutter/material.dart';

class AuthenticatePage extends StatefulWidget {
  AuthenticatePage({Key key}) : super(key: key);

  @override
  _AuthenticatePageState createState() => _AuthenticatePageState();
}

class _AuthenticatePageState extends State<AuthenticatePage> {

  bool showSignIn = true;
  void toggleView() {
    setState(() => showSignIn = !showSignIn);
  }

  @override
  Widget build(BuildContext context) {
  return showSignIn ? SignIn(toggleView: toggleView) : Register(toggleView: toggleView,);
  }
}