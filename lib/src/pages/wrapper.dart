import 'package:facebookauth/src/models/user.dart';
import 'package:facebookauth/src/pages/authenticate.dart';
import 'package:facebookauth/src/pages/home_page.dart';
// import 'package:facebookauth/src/pages/home_page.dart';s
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class Wrapper extends StatelessWidget {
  const Wrapper({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    final user = Provider.of<User>(context);
    print(user);

    //return either Home or Authenticate widget
    return user == null ? AuthenticatePage() : MyHomePage();
    // if (user == null) {
    //   return AuthenticatePage();
    // } else {
    //   return MyHomePage();
    // }
  }
}