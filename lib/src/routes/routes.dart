import 'package:facebookauth/src/pages/authenticate.dart';
import 'package:facebookauth/src/pages/home_page.dart';
import 'package:flutter/material.dart';

Map<String, WidgetBuilder> getApplicationRoutes() {

  return<String, WidgetBuilder> {
    'home'            : ( BuildContext context ) => MyHomePage(),
    'authenticate'  : ( BuildContext context ) => AuthenticatePage()
  };
}